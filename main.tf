provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.region}"
}

resource "aws_instance" "example" {
  ami           = "ami-51a7aa2d"
  instance_type = "t2.micro"
  subnet_id     = "subnet-5c401e1a" # vpc : vpc-fc26f89b

  # public-ssh-all, launch-wizard-1, http-s-all, atlantis

  vpc_security_group_ids = ["sg-056bce7d", "sg-0688c5488c7e3e65a", "sg-0bc3ada102fe59895"] 
  

  tags {
    Name      = "t-koko-from-gitlab-01"
  }
}